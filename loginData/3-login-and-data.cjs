/*
NOTE: Do not change the name of this file

NOTE: For all file operations use promises with fs. Promisify the fs callbacks rather than use fs/promises.

Q1. Create 2 files simultaneously (without chaining).
Wait for 2 seconds and starts deleting them one after another. (in order)
(Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)


Q2. Create a new file with lipsum data (you can google and get this). 
Do File Read and write data to another file
Delete the original file 
Using promise chaining
*/


/*
function login(user, val) {
    if(val % 2 === 0) {
        return Promise.resolve(user);
    } else {
        return Promise.reject(new Error("User not found"));
    }
}

function getData() {
    return Promise.resolve([
        {
            id: 1, 
            name: "Test",
        },
        {
            id: 2, 
            name: "Test 2",
        }
    ]);
}

function logData(user, activity) {
    // use promises and fs to save activity in some file
}
*/
/*
Q3.
Use appropriate methods to 
A. login with value 3 and call getData once login is successful
B. Write logic to logData after each activity for each user. Following are the list of activities
    "Login Success"
    "Login Failure"
    "GetData Success"
    "GetData Failure"

    Call log data function after each activity to log that activity in the file.
    All logged activity must also include the timestamp for that activity.
    
    You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
    All calls must be chained unless mentioned otherwise.
    
*/

//Q1

const path = require('path');
const fs = require('fs');

createFiles()
.then(() => {
    console.log("All files created");
})
.catch((err) => {
    console.error(err);
})
.then(() => {
    return deleteFiles("file1.json");
})
.then(() => {
    return deleteFiles("file2.json");
})
.catch((err) => {
    console.error(err);
});

function createFiles(){

    let fileCountArray = Array.from(new Array(2).keys());
    let fileArray = fileCountArray.map((fileCount) => {
        return `file${fileCount+1}.json`;
    });
    
    return Promise.all(fileArray.map((file) => {

        return new Promise((resolve, reject) => {

            fs.writeFile(path.join(__dirname,file),`Created ${file}`,function(err){

                if(err){
                    console.log("File not created");
                    reject(err);

                } else {
                    console.log("File created");
                    resolve();
                }

            });
        });
    }));
    
}

function deleteFiles(file){

    setTimeout(() => {

        return new Promise((resolve, reject) => {

            fs.unlink(path.join(__dirname,file),function(err){

                if(err){
                    console.log("File not deleted");
                    reject(err);

                } else {
                    console.log("File deleted");
                    resolve(file);

                }
            });
        });

    },2000);
    
}

//Q2

readFile("lipsum.txt")
.then((fileData) => {
    console.log(fileData);
    return writeFile("lipsumCopy.txt",fileData);

})
.then((fileName) => {
    console.log(`${fileName} created`);
    return deleteFile("lipsum.txt");

})
.then((fileName) => {
    console.log(`${fileName} deleted`);
})
.catch((err) => {
    console.error(err.message);
});

function readFile(fileName){

    return new Promise((resolve,reject) => {

        fs.readFile(path.join(__dirname,fileName),"utf-8",function(err,data) {

            if(err){
                console.log("File not read");
                reject(err);

            } else {
                console.log("File read");
                resolve(data);

            }
        });
    });
}

function writeFile(fileName,fileData){

    return new Promise((resolve,reject) => {

        fs.writeFile(path.join(__dirname,fileName),fileData,function(err){

            if(err){
                console.log("File not written");
                reject(err);

            } else {
                console.log("File written");
                resolve(fileName);

            }
        });
    });
}

function deleteFile(fileName) {

    return new Promise((resolve,reject) => {

        fs.unlink(path.join(__dirname,fileName),function(err) {

            if(err){
                console.log("File not deleted");
                reject(err);

            } else {
                console.log("File deleted");
                resolve(fileName);

            }
        });
    });
}
